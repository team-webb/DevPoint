from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib.auth import logout as logout_
from django.contrib.auth import login as login_
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect

# Create your views here.

def logout(request):
    logout_(request)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login_(request, user)
        return redirect('/')
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
