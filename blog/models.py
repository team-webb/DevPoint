from django.db import models
from ckeditor.fields import RichTextField
from datetime import date

class Category(models.Model):
    name = models.CharField(max_length=16)

    class Meta():
    	verbose_name_plural = 'Categories'

    def __str__(self):
    	return self.name

class Post(models.Model):
    title = models.CharField(max_length=30, default='')
    category = models.ManyToManyField(Category)
    text = RichTextField()
    important = models.BooleanField(default = False)
    created = models.DateField(default=date.today)
    published = models.DateField(default=date.today)
    created_by = models.CharField(max_length=30, default='Anonymous')

    class Meta():
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title