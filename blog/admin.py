from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from .models import Category, Post
from django.db import models
# Register your models here.
admin.site.register(Category)


class PostAdminForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Post
        exclude = ['']


class PostAdmin(admin.ModelAdmin):
	filter_horizontal = ('category',)
	form = PostAdminForm

admin.site.register(Post, PostAdmin)
