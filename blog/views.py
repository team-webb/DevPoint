from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.shortcuts import render

from .models import Post


class Index(ListView):
    template_name = "pages/index.html"
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['posts'] = Post.objects.filter(
            important=True).exclude(published=None)
        return context


class PostDetail(DetailView):
    model = Post
    template_name = "pages/postdetail.html"

    def get_context_data(self, **kwargs):
        context = super(PostDetail, self).get_context_data(**kwargs)
        context['post'] = Post.objects.get(pk=self.kwargs.get('pk', None))
        return context


class About(TemplateView):
    template_name = "pages/about.html"


class Contact(TemplateView):
    template_name = "pages/contact.html"

class Login(TemplateView):
    template_name = "pages/login.html"